#!/usr/bin/bash
set -Eeuo pipefail
shopt -s inherit_errexit

set -x

. scripts/reporter_common.sh

if [[ -z "${BEAKER_JOBIDS:-}" ]]; then
    echo "FAIL: BEAKER_JOBIDS is not set"
    exit 0
fi

if [[ -z "${DW_CHECKOUT}" ]]; then
    echo "FAIL: DW_CHECKOUT is not set"
    exit 1
fi

JOB_NOTIFY=${JOB_NOTIFY:-${JOB_OWNER}}

echo "Getting results from beaker jobs: ${BEAKER_JOBIDS}"
# shellcheck disable=SC2086 # we want word splitting on BEAKER_JOBIDS
for jobid in ${BEAKER_JOBIDS}; do
    success=0
    max_attempt=10
    attempt=1
    while [[ "${success}" -eq 0 ]] && [[ "${attempt}" -le "${max_attempt}" ]]; do
        if [[ "${DRY_RUN:-}" == "1" ]]; then
            mv internal/dry-run-bkr-example.xml "job_${jobid}.xml"
            mv internal/dry-run-bkr-example-retried.xml "job_9967967.xml"
        else
            bkr job-results --prettyxml "${jobid}" > "job_${jobid}.xml"
        fi
        # there are some cases where beaker takes a bit longer to provide completed results
        echo "Verifying job result file from Beaker can be parsed using dummy data:"
        if kcidb_tool create --source beaker --src-nvr test-1.2-3.el10 --nvr test-1.2-3.el10 --checkout test --contact test@test.com --input "job_${jobid}.xml" > /dev/null; then
            success=1
        else
            sleep $((60 * attempt ))
            attempt=$(( attempt + 1 ))
        fi
    done
    if [[ "${success}" -eq 0 ]]; then
        echo "ERROR: couldn't parse beaker job ${jobid}"
        exit 1
    fi
done

echo "Reporting results of beaker jobs ${BEAKER_JOBIDS}"
compose=$(cat job_*.xml | sed -n 's/.*distro="\([a-zA-Z0-9.-]\+\)".*/\1/p' | head -1)
if [[ -z "${compose}" ]]; then
    echo "FAIL: couldn't find beaker compose used by job: ${BEAKER_JOBIDS}"
    exit 1
fi

if [[ -z "${TEST_SOURCE_PACKAGE_NVR:-}" ]]; then
    TEST_SOURCE_PACKAGE_NVR=$(find_compose_pkg -c "${compose}" -p "${TEST_SOURCE_PACKAGE_NAME:?}")
    if [[ -z "${TEST_SOURCE_PACKAGE_NVR}" ]]; then
        echo "FAIL: couldn't find nvr for ${TEST_SOURCE_PACKAGE_NAME} on compose ${compose}"
        exit 1
    fi
fi

TEST_PACKAGE_NVR=${TEST_SOURCE_PACKAGE_NVR/${TEST_SOURCE_PACKAGE_NAME}/${TEST_PACKAGE_NAME:?}}

# allow DW_CHECKOUT to contain command line execution
DW_CHECKOUT=$(eval echo "${DW_CHECKOUT}")
contacts=()
for contact in ${JOB_NOTIFY}; do
    contacts+=(--contact "${contact}"@redhat.com)
done

# find recipeSets that got cloned by beaker-jobwatch and remove them from the original jobs
cloned_recipesets=$(grep "clone of TJ#" job_*.xml | sed 's/.*RS:\([[:digit:]]\+\).*/\1/' || true)
for cloned_rs in ${cloned_recipesets:-}; do
    # remove the recipeSet from the beaker job
    for jobxml in job_*.xml; do
        xmlstarlet ed -L -d "/job/recipeSet[@id=${cloned_rs}]" "${jobxml}"
    done
done

# remove job files where all receipeSets got removed above
for jobxml in job_*.xml; do
    grep recipeSet "${jobxml}" || rm -f "${jobxml}"
done

build_issuer=()
build_report_rules=()
if [[ "${REPORT_OSCI:-}" == "1" ]] && [[ -n "${OSCI_REPORT_RULES:-}" ]] && [[ "${DRY_RUN:-}" != "1" ]]; then
    koji --noauth --server "${KOJI_SERVER:?}" call --json-output getBuild "${TEST_SOURCE_PACKAGE_NVR}" > build.json
    issuer=$(jq -r ".extra.custom_user_metadata.osci.upstream_owner_name" < build.json)
    if [[ "${issuer}" == "null" ]]; then
        issuer=$(jq -r ".owner_name" < build.json)
    fi
    build_issuer+=(--submitter "${issuer}@redhat.com")
    build_report_rules+=(--report-rules "${OSCI_REPORT_RULES}")
fi

index=0
merge_results=()
for jobxml in job_*.xml; do
    # some job submiter like workflow tomorrow adds the fetch url as task name, let's remove it, before submiting to DW
    sed -i -E 's/(.*task name.*)(http.*#)/\1/' "${jobxml}"
    sed -i -E 's/(.*task name.*)(git.*#)/\1/' "${jobxml}"
    kcidb_tool create --source beaker --src-nvr "${TEST_SOURCE_PACKAGE_NVR}" --nvr "${TEST_PACKAGE_NVR}" "${contacts[@]}" --checkout-origin "${TEST_JOB_NAME:?}" --builds-origin "${TEST_JOB_NAME:?}" --tests-origin "${TEST_JOB_NAME:?}" -i "${jobxml}" -c "${DW_CHECKOUT}" -o "output${index}.json" --tests-provisioner-url "${CI_PIPELINE_URL:?}" "${build_issuer[@]}" "${build_report_rules[@]}"
    merge_results+=(-r "output${index}.json")
    index=$(( index+1 ))
done
kcidb_tool merge "${merge_results[@]}" -o output.json
# submit results to DataWarehouse
submit2dw
