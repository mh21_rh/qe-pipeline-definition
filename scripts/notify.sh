#!/usr/bin/bash
set -Eeuo pipefail
shopt -s inherit_errexit

set -x

if [[ ! -f slack_message.txt ]]; then
    echo "No slack_message.txt, skipping"
    exit 0
fi

cat slack_message.txt

if [[ "${DRY_RUN:-}" != "1" ]]; then
    send_slack_notification --message-file slack_message.txt
fi
