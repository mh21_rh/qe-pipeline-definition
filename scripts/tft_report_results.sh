#!/usr/bin/bash
set -Eeuo pipefail
shopt -s inherit_errexit

set -x
. scripts/reporter_common.sh

cleanup() {
    {
        echo TEST_PACKAGE_NAME="${TEST_PACKAGE_NAME:-}"
        echo TEST_SOURCE_PACKAGE_NAME="${TEST_SOURCE_PACKAGE_NAME:-}"
        echo TEST_PACKAGE_NVR="${TEST_PACKAGE_NVR:-}"
        echo TEST_SOURCE_PACKAGE_NVR="${TEST_SOURCE_PACKAGE_NVR:-}"
        echo TEST_ARCH="${TEST_ARCH:-}"
    } > test_variables.env
}

trap cleanup EXIT

if [[ -z "${TFT_REQUEST_ID:-}" ]]; then
    echo "FAIL: TFT_REQUEST_ID is not set"
    exit 0
fi
JOB_NOTIFY=${JOB_NOTIFY:-${JOB_OWNER}}

echo "reporting results of testing farm request ${TFT_REQUEST_ID}"
if [[ "${DRY_RUN:-}" == "1" ]]; then
    mv internal/dry-run-tft-example.xml  results.xml
else
    curl --retry 50 "${TFT_API_URL:?}"/requests/"${TFT_REQUEST_ID}" > request_status.json
    xunit_url=$(jq -r '.result.xunit_url' < request_status.json)
    if [[ -z "${xunit_url}" ]]; then
        echo "FAIL: couldn't get xunit url"
        exit 1
    fi

    if [[ "${xunit_url}" == "null" ]]; then
        echo "FAIL: xunit url is null, likely an ifra errror. There is nothing to report to DataWarehouse"
        exit 1
    fi

    # wait some time for testing farm to create the results.xml
    sleep 60
    curl --retry 50 "${xunit_url}" > results.xml
fi

# allow DW_CHECKOUT to contain command line execution
DW_CHECKOUT=$(eval echo "${DW_CHECKOUT}")
contacts=()
for contact in ${JOB_NOTIFY}; do
    contacts+=(--contact "${contact}"@redhat.com)
done

build_issuer=()
build_report_rules=()
if [[ "${REPORT_OSCI:-}" == "1" ]] && [[ -n "${OSCI_REPORT_RULES:-}" ]] && [[ "${DRY_RUN:-}" != "1" ]]; then
    koji --noauth --server "${KOJI_SERVER:?}" call --json-output getBuild "${TEST_SOURCE_PACKAGE_NVR}" > build.json
    issuer=$(jq -r ".extra.custom_user_metadata.osci.upstream_owner_name" < build.json)
    if [[ "${issuer}" == "null" ]]; then
        issuer=$(jq -r ".owner_name" < build.json)
    fi
    build_issuer+=(--submitter "${issuer}@redhat.com")
    build_report_rules+=(--report-rules "${OSCI_REPORT_RULES}")
fi

kcidb_tool create --source testing-farm --src-nvr "${TEST_SOURCE_PACKAGE_NVR}" --nvr "${TEST_PACKAGE_NVR}" "${contacts[@]}" --checkout-origin "${TEST_JOB_NAME:?}" --builds-origin "${TEST_JOB_NAME:?}" --tests-origin "${TEST_JOB_NAME:?}" -i results.xml -c "${DW_CHECKOUT}" -o output.json --tests-provisioner-url "${CI_PIPELINE_URL:?}" "${build_issuer[@]}" "${build_report_rules[@]}"
# submit results to DataWarehouse
submit2dw
