#!/usr/bin/bash
set -Eeuo pipefail
shopt -s inherit_errexit

cleanup() {
    {
        echo BEAKER_JOBIDS="${BEAKER_JOBIDS:-}"
        echo TEST_PACKAGE_NAME="${TEST_PACKAGE_NAME:-}"
        echo TEST_SOURCE_PACKAGE_NAME="${TEST_SOURCE_PACKAGE_NAME:-}"
        echo TEST_PACKAGE_NVR="${TEST_PACKAGE_NVR:-}"
        echo TEST_SOURCE_PACKAGE_NVR="${TEST_SOURCE_PACKAGE_NVR:-}"
        echo TEST_ARCH="${TEST_ARCH:-}"
    } > test_variables.env
}

trap cleanup EXIT

wait_jobs() {
    if [[ -z "${BEAKER_JOBIDS:-}" ]]; then
        echo "FAIL: BEAKER_JOBIDS is not set"
        exit 0
    fi
    echo "Waiting for beaker jobs to finish: ${BEAKER_JOBIDS}"

    # When testing composes, there are cases we don't know the NVR until
    # beaker jobs finishes, only send "running" if we already know the NVR.
    if [[ "${REPORT_OSCI:-}" == "1" ]] && [[ -n "${TEST_SOURCE_PACKAGE_NVR:-}" ]]; then
        # these variables need to be exported as the script uses them
        export TEST_PACKAGE_NAME
        export TEST_ARCH
        # try to send the message, but don't fail the script if there is any problem.
        OSCI_TEST_STATUS=running scripts/osci_report_results.sh || true
    fi

    TEST_JOB_TIMEOUT_HOURS=${TEST_JOB_TIMEOUT_HOURS:-24}

    # wait for 24hrs for jobs to complete
    wait_start=$(date +%s)
    max_time=$(( TEST_JOB_TIMEOUT_HOURS*3600 ))

    # if we know the nvr that will be tested we can submit a test plan
    if [[ -n "${TEST_SOURCE_PACKAGE_NVR:-}" ]]; then
        kcidb_tool create-test-plan --arch "${TEST_ARCH:?}" --src-nvr "${TEST_SOURCE_PACKAGE_NVR}" --nvr "${TEST_PACKAGE_NVR:?}" --checkout-origin "${TEST_JOB_NAME:?}" --builds-origin "${TEST_JOB_NAME:?}" -c "${DW_CHECKOUT:?}" -o test-plan.json
    fi

    if [[ "${DRY_RUN:-}" == "1" ]]; then
        return
    fi

    if [[ -f test-plan.json ]]; then
        kcidb_tool push2umb -i test-plan.json --certificate /tmp/umb_certificate.pem
    fi

    # in case of failures connecting to beaker `bkr job-watch` might fail
    # we should retry it and continue waiting for up to 24hrs since original
    # start time.
    # don't fail script if bkr job-watch doesn't return 0 as this is normal if some task doesn't succeed
    set +e
    # shellcheck disable=SC2312 # Consider invoking this command separately to avoid masking its return value
    while [[ $(( $(date +%s) - max_time )) -lt "${wait_start}" ]]; do
        jobids=${BEAKER_JOBIDS// /,}
        jobwatch_params=(--job "${jobids}")
        if [[ -n "${JOB_OWNER:-}" ]]; then
            jobwatch_params+=(--job-owner "${JOB_OWNER}")
        fi
        # Retry recipe that are queued for more than half of the time expected to run the test
        QUEUED_TIMEOUT_SECONDS=$((TEST_JOB_TIMEOUT_HOURS * 3600 / 2))
        timeout "${TEST_JOB_TIMEOUT_HOURS}"h beaker-jobwatch --queue-timeout "${QUEUED_TIMEOUT_SECONDS}" "${jobwatch_params[@]}" | tee beaker-jobwatch.txt
        if [[ ${PIPESTATUS[0]} == 124 ]]; then
            echo "WARN: tests took over ${TEST_JOB_TIMEOUT_HOURS} hours to run, give up waiting for completion..."
            # update BEAKER_JOBIDS as beaker-jobwatch can create new jobs
            # shellcheck disable=SC2155 # Declare and assign separately to avoid masking return values.
            export BEAKER_JOBIDS=$(grep -oP 'job_ids=.*' < beaker-jobwatch.txt | tail -1 | sed 's/job_ids=/J:/' | sed 's/\+/ J:/g')
            # shellcheck disable=SC2086 # we want word splitting on BEAKER_JOBIDS
            bkr job-cancel ${BEAKER_JOBIDS}
            break
        fi

        all_jobs_ended=1
        # shellcheck disable=SC2086 # we want word splitting on BEAKER_JOBIDS
        for jobid in ${BEAKER_JOBIDS}; do
            bkr job-results --prettyxml "${jobid}" > "job_${jobid}.xml"
            # Make sure the job actually ended
            job_status=$(sed -n 's/.*status="\([a-zA-Z]\+\)".*/\1/p' < "job_${jobid}.xml" | tail -1)
            if [[ "${job_status}" != "Completed" ]] &&
               [[ "${job_status}" != "Aborted" ]] &&
               [[ "${job_status}" != "Cancelled" ]]; then
                    all_jobs_ended=0
                    break
            fi
        done
        if [[ "${all_jobs_ended}" -eq 1 ]]; then
            break
        fi
    done

    # update BEAKER_JOBIDS as beaker-jobwatch can create new jobs
    # shellcheck disable=SC2155 # Declare and assign separately to avoid masking return values.
    export BEAKER_JOBIDS=$(grep -oP 'job_ids=.*' < beaker-jobwatch.txt | tail -1 | sed 's/job_ids=/J:/' | sed 's/\+/ J:/g')

    set -xe
}

bkr_get_compose() {
    tags=""
    if [[ -n "${TEST_COMPOSE_TAGS:-}" ]]; then
        tags="--tag ${TEST_COMPOSE_TAGS// / --tag }"
    fi

    # shellcheck disable=SC2086 # Double quote to prevent globbing
    bkr distros-list --name "${TEST_COMPOSE:?}" ${tags} --limit 1 | tee compose.txt
    BKR_DISCOVERED_COMPOSE=$(grep "Name:" compose.txt | awk '{print$2}')
    export BKR_DISCOVERED_COMPOSE
    return 0
}
