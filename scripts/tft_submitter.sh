#!/usr/bin/bash
set -Eeuo pipefail
shopt -s inherit_errexit

set -x
# remove the / that are passed with TEST_PACKAGE_NAME
TEST_PACKAGE_NAME_ARCH="${TEST_PACKAGE_NAME_ARCH//\/}"
# shellcheck disable=SC2001 # See if you can use ${variable//search/replace} instead.
TEST_PACKAGE_NAME=$(echo "${TEST_PACKAGE_NAME_ARCH}" | sed 's/\(.*\)-\(.*\)/\1/g')
# shellcheck disable=SC2001 # See if you can use ${variable//search/replace} instead.
TEST_ARCH=$(echo "${TEST_PACKAGE_NAME_ARCH}" | sed 's/\(.*\)-\(.*\)/\2/g')
TEST_JOB_TIMEOUT_HOURS=${TEST_JOB_TIMEOUT_HOURS:-12}
TEST_JOB_TIMEOUT_MINS=$(( TEST_JOB_TIMEOUT_HOURS*60 ))


tft_params=("--arch" "${TEST_ARCH}"
            "--context" "component=${TEST_SOURCE_PACKAGE_NAME}"
            "--context" "distro=${TFT_DISTRO}"
            "--context" "package-name=${TEST_PACKAGE_NAME}"
            "--git-url" "${TFT_PLAN_URL}" "--plan" "${TFT_PLAN}"
            "--timeout" "${TEST_JOB_TIMEOUT_MINS}")

if [[ -n "${TEST_COMPOSE:-}" ]]; then
    if [[ -z "${AUTOMOTIVE_RELEASE:-}" ]]; then
        if [[ "${DRY_RUN:-}" != "1" ]]; then
            # we need to know the real compose that is going to be used as in some cases we will need to know the nvr
            # of a package in the compose under test.
            curl --retry 50 "${TFT_OSCI_STORAGE:?}/composes-production.json" > composes-production.json
            real_compose=$(jq -r '[."SYMBOLIC_COMPOSES"[] | select(."'"${TEST_COMPOSE}"'")][0] | ."'"${TEST_COMPOSE}"'"' < composes-production.json)

            # if not found, assume the compose was already a real one.
            if [[ "${real_compose}" != "null" ]]; then
                TEST_COMPOSE=${real_compose}
            fi
        fi
    fi
    tft_params+=("--compose" "${TEST_COMPOSE}")
# Try to get an automotive compose
elif [[ -n "${AUTOMOTIVE_WEBSERVER_RELEASES:-}" && -n "${AUTOMOTIVE_RELEASE}" ]]; then
    automotive_params=("--arch" "${TEST_ARCH}"
                       "--webserver-releases" "${AUTOMOTIVE_WEBSERVER_RELEASES}"
                       "--release" "${AUTOMOTIVE_RELEASE}")
    if [[ -n "${AUTOMOTIVE_HW_TARGET:-}" ]]; then
        automotive_params+=("--hw-target" "${AUTOMOTIVE_HW_TARGET}")
    fi
    if [[ -n "${AUTOMOTIVE_IMAGE_TYPE:-}" ]]; then
        automotive_params+=("--image-type" "${AUTOMOTIVE_IMAGE_TYPE}")
    fi
    if [[ -n "${AUTOMOTIVE_NAME:-}" ]]; then
        automotive_params+=("--image-name" "${AUTOMOTIVE_IMAGE_NAME}")
    fi
    echo "INFO: Getting automotive compose"
    TEST_COMPOSE=$(get_automotive_tf_compose "${automotive_params[@]}")
    if [[ -z "${TEST_COMPOSE}" ]]; then
        echo "FAIL: Couldn't get automotive compose."
        echo "FAIL: Automotive params:" "${automotive_params[@]}"
        exit 1
    else
        echo "INFO: Automotive compose ${TEST_COMPOSE} found."
    fi
    tft_params+=("--compose" "${TEST_COMPOSE}")
else
    echo "FAIL: No compose or automotive variables to get compose specified."
    exit 1
fi

if [[ -n "${TEST_SOURCE_PACKAGE_NVR:-}" ]]; then
    TEST_PACKAGE_NVR=${TEST_SOURCE_PACKAGE_NVR/${TEST_SOURCE_PACKAGE_NAME}/${TEST_PACKAGE_NAME:?}}
    if [[ "${DRY_RUN:-}" == "1" ]]; then
        brew_task_id="123456"
    else
        brew_task_id=$(koji --noauth --server "${KOJI_SERVER:?}" call --json-output getBuild "${TEST_SOURCE_PACKAGE_NVR}" | jq .task_id)
    fi
    if [[ ! "${brew_task_id}" =~ ^[0-9]+$ ]]; then
        echo "FAIL: couldn't get brew task id of ${TEST_SOURCE_PACKAGE_NVR}."
        exit 1
    fi
    tft_params+=("--redhat-brew-build" "${brew_task_id}")
fi

if [[ -z "${TEST_SOURCE_PACKAGE_NVR:-}" ]]; then
    # still not supported to get nvr from automotive compose
    if [[ -n "${AUTOMOTIVE_RELEASE:-}" ]]; then
        TEST_SOURCE_PACKAGE_NVR="TODO"
    else
        TEST_SOURCE_PACKAGE_NVR=$(find_compose_pkg -c "${TEST_COMPOSE}" -p "${TEST_SOURCE_PACKAGE_NAME:?}")
        if [[ -z "${TEST_SOURCE_PACKAGE_NVR}" ]]; then
            echo "FAIL: couldn't find nvr for ${TEST_SOURCE_PACKAGE_NAME} on compose ${TEST_COMPOSE}"
            exit 1
        fi
    fi
fi

TEST_PACKAGE_NVR=${TEST_SOURCE_PACKAGE_NVR/${TEST_SOURCE_PACKAGE_NAME}/${TEST_PACKAGE_NAME:?}}

if [[ -n "${TFT_CONTEXT:-}" ]]; then
    tft_params+=("--context" "${TFT_CONTEXT}")
fi

if [[ -n "${TFT_HARDWARE:-}" ]]; then
    IFS=';' read -r -a hw_array <<< "${TFT_HARDWARE}"
    for value in "${hw_array[@]}"; do
        # Split the value into the key and the condition to handle strings with embedded quote
        # first space character splits key and condition
        tft_params+=("--hardware" "${value/ /=}")
    done
fi

if [[ -n "${TFT_PARAMS:-}" ]]; then
    IFS=';' read -r -a param_array <<< "${TFT_PARAMS}"
    for param in "${param_array[@]}"; do
        for value in ${param}; do
            tft_params+=("${value}")
        done
    done
fi

if [[ -n "${TFT_POOL:-}" ]]; then
    tft_params+=("--pool" "${TFT_POOL}")
fi

if [[ "${REPORT_POLARION:-0}" -eq "1" ]]; then
    # https://docs.testing-farm.io/Testing%20Farm/0.1/test-request.html#polarion
    # https://tmt.readthedocs.io/en/stable/plugins/report.html#polarion
    tft_params+=("--tmt-environment" "POLARION_REPO=${POLARION_BASE_URL:?}/repo/")
    tft_params+=("--tmt-environment" "POLARION_URL=${POLARION_BASE_URL:?}/polarion/")
    tft_params+=("--tmt-environment" "POLARION_PROJECT=${POLARION_PROJECT:?}")
    tft_params+=("--tmt-environment" "POLARION_USERNAME=${POLARION_USER:?}")
    tft_params+=("--tmt-environment" "POLARION_PASSWORD=${POLARION_PASSWORD:?}")
    tft_params+=("--tmt-environment" "TMT_PLUGIN_REPORT_POLARION_PROJECT_ID=${POLARION_PROJECT:?}")
    tft_params+=("--tmt-environment" "TMT_PLUGIN_REPORT_POLARION_TITLE=${POLARION_RUN_TITLE:?}")
    tft_params+=("--tmt-environment" "TMT_PLUGIN_REPORT_POLARION_PLANNED_IN=${POLARION_TEST_PHASE:?}")
    tft_params+=("--tmt-environment" "TMT_PLUGIN_REPORT_POLARION_ASSIGNEE=${POLARION_OWNER:?}")
    tft_params+=("--context" "polarion_report=on")
fi

if [[ "${DRY_RUN:-}" == "1" ]]; then
    tft_params+=("--dry-run")
fi

# shellcheck disable=SC2086,SC2048 # Double quote to prevent globbing - Use "$@" (with quotes) to prevent whitespace problems
testing-farm request --no-wait --kickstart "kernel-options=enforcing=0" --kickstart "'kernel-options-post=enforcing=0 rd.emergency=reboot'" \
    "${tft_params[@]}" | tee request_output.txt

if [[ "${DRY_RUN:-}" == "1" ]]; then
    echo "INFO: running as dry run, no job is created."
    echo "api https://api.dev.testing-farm.io/v0.1/requests/da7aa8a6-04c5-4e70-bbbc-cdeafe6bb732" | tee request_output.txt
fi
TFT_REQUEST_ID=$(grep "api" request_output.txt | grep -o '[^/]*$')
if [[ -z "${TFT_REQUEST_ID}" ]]; then
    echo "FAIL: Couldn't submit testing-farm request."
    exit 1
fi

{
    echo TFT_REQUEST_ID="${TFT_REQUEST_ID}"
    echo TEST_PACKAGE_NAME="${TEST_PACKAGE_NAME}"
    echo TEST_PACKAGE_NVR="${TEST_PACKAGE_NVR:-}"
    echo TEST_SOURCE_PACKAGE_NAME="${TEST_SOURCE_PACKAGE_NAME:?}"
    echo TEST_ARCH="${TEST_ARCH}"
} > test_variables.env

# When testing composes, there are cases we don't know the NVR until
# testing-farm jobs finishes, only send "running" if we already know the NVR.
if [[ "${REPORT_OSCI:-}" == "1" ]] && [[ -n "${TEST_SOURCE_PACKAGE_NVR:-}" ]]; then
    # these variables need to be exported as the script uses them
    export TEST_PACKAGE_NAME
    export TEST_ARCH
    # try to send the message, but don't fail the script if there is any problem.
    OSCI_TEST_STATUS=running scripts/osci_report_results.sh || true
fi

if [[ -n "${TEST_SOURCE_PACKAGE_NVR:-}" ]]; then
    kcidb_tool create-test-plan --arch "${TEST_ARCH:?}" --src-nvr "${TEST_SOURCE_PACKAGE_NVR:?}" --nvr "${TEST_PACKAGE_NVR:?}" --checkout-origin "${TEST_JOB_NAME:?}" --builds-origin "${TEST_JOB_NAME:?}" -c "${DW_CHECKOUT:?}" -o test-plan.json
fi

# don't fail pipeline if a command fails
set +e
# wait for 24hrs for jobs to complete
wait_start=$(date +%s)
max_time=$(( TEST_JOB_TIMEOUT_HOURS*3600 ))

if [[ "${DRY_RUN:-}" == "1" ]]; then
    exit 0
fi

if [[ -f test-plan.json ]]; then
    kcidb_tool push2umb -i test-plan.json --certificate /tmp/umb_certificate.pem
fi

# in case of failures connecting to beaker `bkr job-watch` might fail
# we should retry it and continue waiting for up to 24hrs since original
# start time.
# shellcheck disable=SC2312 # Consider invoking this command separately to avoid masking its return value
while [[ $(( $(date +%s) - max_time )) -lt "${wait_start}" ]]; do
    timeout "${TEST_JOB_TIMEOUT_HOURS}"h testing-farm watch --id "${TFT_REQUEST_ID}"
    if [[ $? == 124 ]]; then
        echo "FAIL: tests took over ${TEST_JOB_TIMEOUT_HOURS} hours to run, give up waiting for completion..."
        testing-farm cancel "${TFT_REQUEST_ID}"
        exit 1
    fi

    # make sure watch returned, because request completed and not due some infra problem
    curl --retry 50 "${TFT_API_URL:?}"/requests/"${TFT_REQUEST_ID}" > request_status.json
    state=$(jq -r ".state" < request_status.json)
    if [[ "${state}" == "complete" ]] ||
       [[ "${state}" == "error" ]]; then
        break
    fi
done
set -e
