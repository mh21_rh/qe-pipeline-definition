#!/usr/bin/bash

# shellcheck disable=SC2317 # Command appears to be unreachable

# shellcheck disable=SC2312 # Consider invoking this command separately to avoid masking its return value
eval "$(shellspec - -c) exit 1"

Describe 'tft_report'
    export JOB_NOTIFY=test
    export TEST_PACKAGE_NAME_ARCH=kernel-debug-x86_64
    export TEST_PACKAGE_NAME=kernel-debug
    export TEST_PACKAGE_NVR=kernel-debug-5.14.0.mock.el10
    export TEST_SOURCE_PACKAGE_NAME=kernel
    export TEST_SOURCE_PACKAGE_NVR=kernel-5.14.0.mock.el10
    export TEST_JOB_NAME=mytest
    export TFT_REQUEST_ID=mocked-tft-id
    export TFT_API_URL=test-api
    # shellcheck disable=SC2016 # Expressions don't expand in single quotes, use double quotes for that.
    export DW_CHECKOUT='dw checkout: $(cmd)'
    export mocked_cmd="Wed May 22"
    export CI_PIPELINE_URL=test_url
    export xunit_result="https://mock.com"

    cleanup() {
        rm -rf results.xml
    }
    After 'cleanup'


    Mock sleep
        exit 0
    End

    Mock cmd
        echo "${mocked_cmd}"
    End

    Mock testing-farm
        exit 0
    End

    Mock kcidb_tool
        echo "kcidb_tool $*"
    End

    Mock koji
        echo "${buildinfo}"
    End

    Mock curl
        if [[ "$*" =~ "requests" ]]; then
            echo "{\"state\":\"complete\", \"result\":{\"xunit_url\":\"${xunit_result}\"}}"
        elif [[ "$*" =~ https://mock.com ]]; then
            echo '<property name="compose" value="RHEL-9.5.0-Nightly"/>'
            echo '<property name="compose" value="RHEL-9.5.0-Nightly"/>'
            echo '<property name="compose" value="RHEL-9.5.0-Nightly"/>'
        else
            echo "$@"
        fi
    End

    Mock python3
        echo "python3 $*"
    End

    It 'can report results'
        When run script scripts/tft_report_results.sh
        The status should be success
        The stdout should include "reporting results of testing farm request ${TFT_REQUEST_ID}"
        The stderr should include "kcidb_tool create --source testing-farm --src-nvr kernel-5.14.0.mock.el10 --nvr kernel-debug-5.14.0.mock.el10 --contact test@redhat.com --checkout-origin mytest --builds-origin mytest --tests-origin mytest -i results.xml -c 'dw checkout: Wed May 22' -o output.json --tests-provisioner-url test_url"
        The stdout should include "kcidb_tool push2umb -i output.json --certificate /tmp/umb_certificate.pem"
        The stdout should include "python3 -m cki_tools.datawarehouse_submitter --kcidb-file ./output.json"
        The stderr should include "curl --retry 50 https://mock.com"
    End

    It 'can exit with failure if xunit_url is null'
        export xunit_result="null"
        When run script scripts/tft_report_results.sh
        The status should be failure
        The stdout should include "reporting results of testing farm request ${TFT_REQUEST_ID}"
        The stdout should include "FAIL: xunit url is null, likely an ifra errror. There is nothing to report to DataWarehouse"
        The stderr should not equal ""
    End

    It 'can report results with REPORT_OSCI - build owner'
        export REPORT_OSCI=1
        export KOJI_SERVER=test.koji.com
        export OSCI_REPORT_RULES='[{"when": "always", "send_to": ["submitter"]}]'
        export buildinfo='{"name": "buildname", "task_id": "taskid", "owner_name": "owner_name", "source": "source"}'
        When run script scripts/tft_report_results.sh
        The status should be success
        The stdout should include "reporting results of testing farm request ${TFT_REQUEST_ID}"
        The stderr should include "kcidb_tool create --source testing-farm --src-nvr kernel-5.14.0.mock.el10 --nvr kernel-debug-5.14.0.mock.el10 --contact test@redhat.com --checkout-origin mytest --builds-origin mytest --tests-origin mytest -i results.xml -c 'dw checkout: Wed May 22' -o output.json --tests-provisioner-url test_url --submitter owner_name@redhat.com --report-rules '[{\"when\": \"always\", \"send_to\": [\"submitter\"]}]'"
        The stdout should include "kcidb_tool push2umb -i output.json --certificate /tmp/umb_certificate.pem"
        The stderr should include "curl --retry 50 https://mock.com"
    End

    It 'can report results with REPORT_OSCI - upstream owner'
        export REPORT_OSCI=1
        export KOJI_SERVER=test.koji.com
        export OSCI_REPORT_RULES='[{"when": "always", "send_to": ["submitter"]}]'
        export buildinfo='{"name": "buildname", "task_id": "taskid", "owner_name": "owner_name", "source": "source", "extra": {"custom_user_metadata": {"osci": {"upstream_owner_name": "owner_name_upstream"}}}}'
        When run script scripts/tft_report_results.sh
        The status should be success
        The stdout should include "reporting results of testing farm request ${TFT_REQUEST_ID}"
        The stderr should include "kcidb_tool create --source testing-farm --src-nvr kernel-5.14.0.mock.el10 --nvr kernel-debug-5.14.0.mock.el10 --contact test@redhat.com --checkout-origin mytest --builds-origin mytest --tests-origin mytest -i results.xml -c 'dw checkout: Wed May 22' -o output.json --tests-provisioner-url test_url --submitter owner_name_upstream@redhat.com --report-rules '[{\"when\": \"always\", \"send_to\": [\"submitter\"]}]'"
        The stdout should include "kcidb_tool push2umb -i output.json --certificate /tmp/umb_certificate.pem"
        The stderr should include "curl --retry 50 https://mock.com"
    End
End
