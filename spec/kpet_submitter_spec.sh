#!/usr/bin/bash

# shellcheck disable=SC2317 # Command appears to be unreachable

# shellcheck disable=SC2312 # Consider invoking this command separately to avoid masking its return value
eval "$(shellspec - -c) exit 1"

Describe 'kpet_submitter'
    cleanup() {
        rm -rf test_variables.env
    }
    AfterEach 'cleanup'

    export DW_CHECKOUT="mock-checkout"
    export TEST_JOB_NAME="mock-testjob"
    export TEST_PACKAGE_NAME_ARCH=kernel-rt-debug-x86_64
    export TEST_SOURCE_PACKAGE_NAME=kernel-rt
    export KPET_DB_URL=mocked_kpet_db_url
    export KPET_TREE=tree-mock
    export KPET_SET=set-mock
    # shellcheck disable=SC2016 # Expressions don't expand in single quotes, use double quotes for that.
    export KPET_DESCRIPTION='date: $(mock_date)'
    export mocked_date="Wed May 22 11:22:03 UTC 2024"

    export bkr_output="J:123456"

    Mock git
        echo "git $*"
    End

    Mock kpet
        exit 0
    End

    Mock bkr
        if [[ "$1" == "job-submit" ]]; then
            echo "${bkr_output}"
        elif [[ "$1" == "job-results" ]]; then
            echo 'status="Completed"'
            echo 'distro="mocked-distro"'
            echo 'status="Completed"'
            echo 'distro="mocked-distro"'
            echo 'status="Completed"'
        else
            echo "fail, unsupported bkr command $1"
            exit 1
        fi
    End

    Mock mock_date
        echo "${mocked_date}"
    End

    Mock beaker-jobwatch
        echo "toggle_nacks_on=on&job_ids=123456"
    End

    Mock kcidb_tool
        echo "kcidb_tool ${*}"
    End

    It 'can submit job'
        When run script scripts/kpet_submitter.sh
        The status should be success
        The stdout should include "git clone --depth 1 mocked_kpet_db_url kpet-db"
        The stderr should include "kpet --db kpet-db run generate --tree tree-mock --arch x86_64 --set set-mock --description 'date: ${mocked_date}' --output beaker.xml --components 'rt|debugbuild'"
        The stderr should include "wait_jobs"
        The contents of file test_variables.env should include "BEAKER_JOBIDS=J:123456"
        The contents of file test_variables.env should include "TEST_PACKAGE_NAME=kernel-rt-debug"
        The contents of file test_variables.env should include "TEST_SOURCE_PACKAGE_NAME=kernel-rt"
        The contents of file test_variables.env should include "TEST_ARCH=x86_64"
    End

    It 'can submit job with JOB_OWNER'
        export JOB_OWNER=testowner
        When run script scripts/kpet_submitter.sh
        The status should be success
        The stdout should include "git clone --depth 1 mocked_kpet_db_url kpet-db"
        The stderr should include "kpet --db kpet-db run generate --tree tree-mock --arch x86_64 --set set-mock --description 'date: ${mocked_date}' --output beaker.xml --components 'rt|debugbuild'"
        The stderr should include "wait_jobs"
        The stderr should include "bkr job-submit beaker.xml --job-owner testowner"
        The contents of file test_variables.env should include "BEAKER_JOBIDS=J:123456"
        The contents of file test_variables.env should include "TEST_PACKAGE_NAME=kernel-rt-debug"
        The contents of file test_variables.env should include "TEST_SOURCE_PACKAGE_NAME=kernel-rt"
        The contents of file test_variables.env should include "TEST_ARCH=x86_64"
    End

    It 'can submit job with KPET_COMPONENT set'
        export KPET_COMPONENT="officialbuild"
        When run script scripts/kpet_submitter.sh
        The status should be success
        The stdout should include "git clone --depth 1 mocked_kpet_db_url kpet-db"
        The stderr should include "kpet --db kpet-db run generate --tree tree-mock --arch x86_64 --set set-mock --description 'date: ${mocked_date}' --output beaker.xml --components 'officialbuild|rt|debugbuild'"
        The stderr should include "wait_jobs"
        The contents of file test_variables.env should include "BEAKER_JOBIDS=J:123456"
        The contents of file test_variables.env should include "TEST_PACKAGE_NAME=kernel-rt-debug"
        The contents of file test_variables.env should include "TEST_SOURCE_PACKAGE_NAME=kernel-rt"
        The contents of file test_variables.env should include "TEST_ARCH=x86_64"
    End

    It 'can submit job with 64kpages component set'
        export TEST_PACKAGE_NAME_ARCH=kernel-64k-aarch64
        export KPET_COMPONENT="officialbuild"
        When run script scripts/kpet_submitter.sh
        The status should be success
        The stdout should include "git clone --depth 1 mocked_kpet_db_url kpet-db"
        The stderr should include "kpet --db kpet-db run generate --tree tree-mock --arch aarch64 --set set-mock --description 'date: ${mocked_date}' --output beaker.xml --components 'officialbuild|64kpages'"
        The stderr should include "wait_jobs"
        The contents of file test_variables.env should include "BEAKER_JOBIDS=J:123456"
        The contents of file test_variables.env should include "TEST_PACKAGE_NAME=kernel-64k"
        The contents of file test_variables.env should include "TEST_SOURCE_PACKAGE_NAME=kernel"
        The contents of file test_variables.env should include "TEST_ARCH=aarch64"
    End

    It 'can submit job passing kernel build nvr as parameter'
        export BREW_TASK_REPOS_URL="https://brew-task.test/repos"
        export TEST_SOURCE_PACKAGE_NAME=kernel
        export TEST_SOURCE_PACKAGE_NVR=kernel-6.11-2.el10
        When run script scripts/kpet_submitter.sh
        The status should be success
        The stdout should include "git clone --depth 1 mocked_kpet_db_url kpet-db"
        The stderr should include "kpet --db kpet-db run generate --tree tree-mock --arch x86_64 --set set-mock --description 'date: ${mocked_date}' --output beaker.xml --components 'rt|debugbuild' -k '${BREW_TASK_REPOS_URL}/official/kernel/6.11/2.el10/\$basearch#package_name=kernel-rt-debug&amp;source_package_name=kernel"
        The stderr should include "wait_jobs"
        The contents of file test_variables.env should include "BEAKER_JOBIDS=J:123456"
        The contents of file test_variables.env should include "TEST_PACKAGE_NAME=kernel-rt-debug"
        The contents of file test_variables.env should include "TEST_SOURCE_PACKAGE_NAME=kernel"
        The contents of file test_variables.env should include "TEST_PACKAGE_NVR=kernel-rt-debug-6.11-2.el10"
        The contents of file test_variables.env should include "TEST_ARCH=x86_64"
    End

    It 'can submit job passing non-kernel build nvr as parameter'
        export TEST_PACKAGE_NAME_ARCH=glibc-x86_64
        export TEST_SOURCE_PACKAGE_NAME=glibc
        export TEST_SOURCE_PACKAGE_NVR=glibc-2.39-22.el10
        When run script scripts/kpet_submitter.sh
        The status should be success
        The stdout should include "git clone --depth 1 mocked_kpet_db_url kpet-db"
        The stderr should include "kpet --db kpet-db run generate --tree tree-mock --arch x86_64 --set set-mock --description 'date: ${mocked_date}' --output beaker.xml -v brew_build_nvr=glibc-2.39-22.el10"
        The stderr should include "wait_jobs"
        The contents of file test_variables.env should include "BEAKER_JOBIDS=J:123456"
        The contents of file test_variables.env should include "TEST_PACKAGE_NAME=glibc"
        The contents of file test_variables.env should include "TEST_SOURCE_PACKAGE_NAME=glibc"
        The contents of file test_variables.env should include "TEST_ARCH=x86_64"
    End
End
