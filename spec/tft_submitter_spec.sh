#!/usr/bin/bash

# shellcheck disable=SC2312 # Consider invoking this command separately to avoid masking its return value
eval "$(shellspec - -c) exit 1"

Describe 'tft_submitter'
    cleanup() {
        rm -rf test_variables.env
    }
    AfterEach 'cleanup'

    export DW_CHECKOUT="mock-checkout"
    export TEST_JOB_NAME="mock-testjob"
    export TEST_PACKAGE_NAME_ARCH=kernel-debug-x86_64
    export TEST_SOURCE_PACKAGE_NAME=kernel
    export TFT_API_URL=test-api
    export TEST_COMPOSE=mocked_compose
    export TFT_DISTRO="rhel-9.5"
    export TFT_OSCI_STORAGE=osci-storage
    export TFT_PLAN_URL=mocked_plan_url
    export TFT_PLAN=mocked_testplan
    export xunit_result="https://mock.com"


    Mock testing-farm
        echo "api https://api.tft-mocked/v0.1/requests/mocked-request-id"
    End

    Mock curl
        if [[ "$*" =~ "requests" ]]; then
            echo "{\"state\":\"complete\", \"result\":{\"xunit_url\":\"${xunit_result}\"}}"
        elif [[ "$*" =~ https://mock.com ]]; then
            echo '<property name="compose" value="RHEL-9.5.0-Nightly"/>'
            echo '<property name="compose" value="RHEL-9.5.0-Nightly"/>'
            echo '<property name="compose" value="RHEL-9.5.0-Nightly"/>'
        elif [[ "$*" =~ composes-production.json ]]; then
            echo '{"SYMBOLIC_COMPOSES": [{ "RHEL-9.5.0-Nightly": "RHEL-9.5.0-20240528.55"}]}'
        else
            echo "$@"
        fi
    End

    Mock kcidb_tool
        echo "kcidb_tool ${*}"
    End

    Mock find_compose_pkg
        echo "kernel-5.14.0.mock.el10"
    End

    Mock koji
        echo '{"state": 1,"task_id": 63290961, "version": "5.14.0"}'
    End

    Mock get_automotive_tf_compose
        echo '{"automotive_info": "some_automotive_info"}'
    End

    Mock result2osci
        echo "result2osci ${*}"
    End

    tft_common_params="--no-wait --kickstart kernel-options=enforcing=0 --kickstart ''\''kernel-options-post=enforcing=0 rd.emergency=reboot'\''' --arch x86_64"
    tft_common_params+=" --context component=kernel --context distro=rhel-9.5 --context package-name=kernel-debug --git-url mocked_plan_url --plan mocked_testplan --timeout 720"
    tft_compose_param="--compose mocked_compose"

    It 'can submit job'
        When run script scripts/tft_submitter.sh
        The status should be success
        The stdout should include "api "
        The stderr should include "find_compose_pkg -c mocked_compose -p kernel"
        The stderr should include "testing-farm request ${tft_common_params} ${tft_compose_param}"
        The stderr should include "timeout 12h testing-farm watch --id mocked-request-id"
        The stderr should include "kcidb_tool create-test-plan --arch x86_64 --src-nvr kernel-5.14.0.mock.el10 --nvr kernel-debug-5.14.0.mock.el10 --checkout-origin mock-testjob --builds-origin mock-testjob -c mock-checkout -o test-plan.json"
        The contents of file test_variables.env should include "TFT_REQUEST_ID=mocked-request-id"
        The contents of file test_variables.env should include "TEST_PACKAGE_NAME=kernel-debug"
        The contents of file test_variables.env should include "TEST_SOURCE_PACKAGE_NAME=kernel"
        The contents of file test_variables.env should include "TEST_ARCH=x86_64"
    End

    It 'can submit job with context'
        export TFT_CONTEXT='test-context'
        When run script scripts/tft_submitter.sh
        The status should be success
        The stdout should include "api "
        The stderr should include "testing-farm request ${tft_common_params} ${tft_compose_param} --context ${TFT_CONTEXT}"
        The contents of file test_variables.env should include "TFT_REQUEST_ID=mocked-request-id"
        The contents of file test_variables.env should include "TEST_PACKAGE_NAME=kernel-debug"
        The contents of file test_variables.env should include "TEST_SOURCE_PACKAGE_NAME=kernel"
        The contents of file test_variables.env should include "TEST_ARCH=x86_64"
    End

    It 'can submit job with hardware'
        export TFT_HARDWARE="cpu.model-name =~ Intel Xeon;disk.space >= 40 GiB"
        When run script scripts/tft_submitter.sh
        The status should be success
        The stdout should include "api "
        The stderr should include "testing-farm request ${tft_common_params} ${tft_compose_param} --hardware 'cpu.model-name==~ Intel Xeon' --hardware 'disk.space=>= 40 GiB'"
        The contents of file test_variables.env should include "TFT_REQUEST_ID=mocked-request-id"
        The contents of file test_variables.env should include "TEST_PACKAGE_NAME=kernel-debug"
        The contents of file test_variables.env should include "TEST_SOURCE_PACKAGE_NAME=kernel"
        The contents of file test_variables.env should include "TEST_ARCH=x86_64"
    End

    It 'can submit job with environment'
        export TFT_PARAMS="--environment KPATCH_REPO=https://github.com/zhjwong/kpatch.git;--environment KPATCH_REV=integration-tests_95_test2"
        When run script scripts/tft_submitter.sh
        The status should be success
        The stdout should include "api "
        The stderr should include "testing-farm request ${tft_common_params} ${tft_compose_param} --environment KPATCH_REPO=https://github.com/zhjwong/kpatch.git --environment KPATCH_REV=integration-tests_95_test2"
        The contents of file test_variables.env should include "TFT_REQUEST_ID=mocked-request-id"
        The contents of file test_variables.env should include "TEST_PACKAGE_NAME=kernel-debug"
        The contents of file test_variables.env should include "TEST_SOURCE_PACKAGE_NAME=kernel"
        The contents of file test_variables.env should include "TEST_ARCH=x86_64"
    End

    It 'can submit job with pool'
        export TFT_POOL="test-pool"
        When run script scripts/tft_submitter.sh
        The status should be success
        The stdout should include "api "
        The stderr should include "testing-farm request ${tft_common_params} ${tft_compose_param} --pool ${TFT_POOL}"
        The contents of file test_variables.env should include "TFT_REQUEST_ID=mocked-request-id"
        The contents of file test_variables.env should include "TEST_PACKAGE_NAME=kernel-debug"
        The contents of file test_variables.env should include "TEST_SOURCE_PACKAGE_NAME=kernel"
        The contents of file test_variables.env should include "TEST_ARCH=x86_64"
    End

    It 'can submit to Polarion'
        export REPORT_POLARION="1"
        export POLARION_BASE_URL="polarion.test.com"
        export POLARION_PROJECT="test-project"
        export POLARION_USER="polarion-user"
        export POLARION_PASSWORD="polarion-password"
        export POLARION_RUN_TITLE="polarion-testrun"
        export POLARION_TEST_PHASE="polarion-testphase"
        export POLARION_OWNER="polarion-testowner"
        When run script scripts/tft_submitter.sh
        The status should be success
        The stdout should include "api "
        The stderr should include "testing-farm request ${tft_common_params} ${tft_compose_param} --tmt-environment POLARION_REPO=polarion.test.com/repo/ --tmt-environment POLARION_URL=polarion.test.com/polarion/ --tmt-environment POLARION_PROJECT=test-project --tmt-environment POLARION_USERNAME=polarion-user --tmt-environment POLARION_PASSWORD=polarion-password --tmt-environment TMT_PLUGIN_REPORT_POLARION_PROJECT_ID=test-project --tmt-environment TMT_PLUGIN_REPORT_POLARION_TITLE=polarion-testrun --tmt-environment TMT_PLUGIN_REPORT_POLARION_PLANNED_IN=polarion-testphase --tmt-environment TMT_PLUGIN_REPORT_POLARION_ASSIGNEE=polarion-testowner --context polarion_report=on"
    End

    It 'can submit job with package nvr'
        export TEST_SOURCE_PACKAGE_NVR="kernel-5.14.0-496.el9"
        export KOJI_SERVER="test-koji"
        When run script scripts/tft_submitter.sh
        The status should be success
        The stdout should include "api "
        The stderr should include "testing-farm request ${tft_common_params} ${tft_compose_param} --redhat-brew-build 63290961"
        The contents of file test_variables.env should include "TFT_REQUEST_ID=mocked-request-id"
        The contents of file test_variables.env should include "TEST_PACKAGE_NAME=kernel-debug"
        The contents of file test_variables.env should include "TEST_SOURCE_PACKAGE_NAME=kernel"
        The contents of file test_variables.env should include "TEST_ARCH=x86_64"
    End

    It 'can submit job and report to OSCI if there is package nvr'
        export KOJI_SERVER="test-koji"
        export CI_PIPELINE_URL="pipeline.test/"
        export CI_PIPELINE_ID="123456"
        export OSCI_TEST_CATEGORY="mycategory"
        export OSCI_TEST_TYPE="tier1"
        export REPORT_OSCI=1
        export TEST_ARCH="x86_64"
        export TEST_PACKAGE_NAME="kernel"
        export TEST_SOURCE_PACKAGE_NVR="kernel-5.14.0-496.el9"
        When run script scripts/tft_submitter.sh
        The status should be success
        The stdout should include "api "
        The stderr should include "testing-farm request ${tft_common_params} ${tft_compose_param}"
        The stderr should include "result2osci --source-nvr kernel-5.14.0-496.el9 --certificate /tmp/umb_certificate.pem --pipeline-id 123456 --log-url pipeline.test/ --test-status running --run-url pipeline.test/ --test-category mycategory --test-namespace kernel-qe.kernel-ci.kernel-debug-x86_64 --test-type tier1"
        The contents of file test_variables.env should include "TFT_REQUEST_ID=mocked-request-id"
        The contents of file test_variables.env should include "TFT_REQUEST_ID=mocked-request-id"
        The contents of file test_variables.env should include "TEST_PACKAGE_NAME=kernel-debug"
        The contents of file test_variables.env should include "TEST_PACKAGE_NVR=kernel-debug-5.14.0-496.el9"
        The contents of file test_variables.env should include "TEST_SOURCE_PACKAGE_NAME=kernel"
        The contents of file test_variables.env should include "TEST_ARCH=x86_64"
    End

    It 'can submit job and NOT report to OSCI if package nvr is NOT set'
        export KOJI_SERVER="test-koji"
        export CI_PIPELINE_URL="pipeline.test/"
        export REPORT_OSCI=1
        When run script scripts/tft_submitter.sh
        The status should be success
        The stdout should include "api "
        The stderr should include "testing-farm request ${tft_common_params} ${tft_compose_param}"
        The stderr should not include "result2osci"
        The contents of file test_variables.env should include "TFT_REQUEST_ID=mocked-request-id"
        The contents of file test_variables.env should include "TFT_REQUEST_ID=mocked-request-id"
        The contents of file test_variables.env should include "TEST_PACKAGE_NAME=kernel-debug"
        The contents of file test_variables.env should include "TEST_ARCH=x86_64"
    End
    It 'can submit job with an automotive compose'
        export TEST_COMPOSE=""
        export AUTOMOTIVE_WEBSERVER_RELEASES="https://automotive.com"
        export AUTOMOTIVE_RELEASE="nightly"
        When run script scripts/tft_submitter.sh
        The status should be success
        The stdout should include "api "
        The stdout should include "INFO: Automotive compose"
        The stderr should include "testing-farm request ${tft_common_params} --compose '{\"automotive_info\": \"some_automotive_info\"}'"
        The stderr should include "timeout 12h testing-farm watch --id mocked-request-id"
        The contents of file test_variables.env should include "TFT_REQUEST_ID=mocked-request-id"
        The contents of file test_variables.env should include "TEST_PACKAGE_NAME=kernel-debug"
        The contents of file test_variables.env should include "TEST_SOURCE_PACKAGE_NAME=kernel"
        The contents of file test_variables.env should include "TEST_ARCH=x86_64"

    End
    It 'can not submit when a TEST_COMPOSE or AUTOMOTIVE_WEBSERVER_RELEASES and AUTOMOTIVE_RELEASE are not set'
        export TEST_COMPOSE=""
        export AUTOMOTIVE_WEBSERVER_RELEASES=""
        export AUTOMOTIVE_RELEASE=""
        When run script scripts/tft_submitter.sh
        The status should be failure
        The stdout should include "FAIL: No compose or automotive variables to get compose specified."
        The stderr should include "FAIL: No compose or automotive variables to get compose specified."
    End
    It 'shows automotive command line arguments to get the compose when the CLI fails'
        export TEST_COMPOSE=""
        export AUTOMOTIVE_WEBSERVER_RELEASES="https://automotive.com"
        export AUTOMOTIVE_RELEASE="nightly"
        Mock get_automotive_tf_compose
            echo ""
        End
        When run script scripts/tft_submitter.sh
        The status should be failure
        The stdout should include "FAIL: Couldn't get automotive compose."
        The stdout should include "FAIL: Automotive params: --arch x86_64 --webserver-releases https://automotive.com --release nightly"
        The stderr should include "--arch x86_64 --webserver-releases https://automotive.com --release nightly"

    End
End
